package com.example.todoapp;

public class Item {

    private String todoNameItem;
    private  String todoContextItem;

    public Item(String todoNameItem, String todoContextItem)
    {
        this.todoNameItem = todoNameItem;
        this.todoContextItem = todoContextItem;
    }

    public String getTodoNameItem() { return todoNameItem; }

    public  String getTodoContextItem() { return todoContextItem; }

    public void setTodoNameItem(String todoNameItem) {this.todoNameItem = todoNameItem; }

    public void setTodoContextItem(String todoContextItem) {this.todoContextItem = todoContextItem; }

} /*Item*/
