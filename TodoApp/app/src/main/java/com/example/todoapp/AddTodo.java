package com.example.todoapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AddTodo extends AppCompatActivity {

    EditText todoName, todoContext;
    Button done;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_todo);

        todoName = findViewById(R.id.todoName);
        todoContext = findViewById(R.id.todoContext);
        done = findViewById(R.id.done);
    }

    public void doneClicked(View view)
    {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("todoName", todoName.getText().toString());
        intent.putExtra("todoContext", todoContext.getText().toString());
        startActivity(intent);
    }
} /*AddTodo*/