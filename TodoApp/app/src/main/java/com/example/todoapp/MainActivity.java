package com.example.todoapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    String todoName;
    String todoContext;
    ListView list;
    ArrayAdapter<String> adapter;
    ArrayList<String> stirngList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listInit();
        getItemContext();
    }

    public void listInit()
    {
        list = findViewById(R.id.list);
        stirngList = new ArrayList<String>();
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, stirngList);

        String[] fruits = new String[] {
                "Cape Gooseberry",
                "Capuli cherry"
        };

        for (int i=0; i<fruits.length;i++)
        {
            stirngList.add(fruits[i]);
        }

        list.setAdapter(adapter);
    }

    public void addTodoClick(View v)
    {
        Intent intent = new Intent(this, AddTodo.class);
        startActivity(intent);
    }

    public void getItemContext()
    {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
        {
            todoName = bundle.getString("todoName");
            todoContext = bundle.getString("todoContext");

            stirngList.add(todoName);
            adapter.notifyDataSetInvalidated();
        }
    }
} /*MainActivity*/