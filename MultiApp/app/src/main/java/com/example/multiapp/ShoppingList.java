package com.example.multiapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class ShoppingList extends AppCompatActivity {

    Button addBtn;
    ListView listView;
    ArrayAdapter arrayAdapter;
    private ArrayList<String> items = new ArrayList<String>();
    EditText itemEt;
    boolean appSwitched;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_list);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN); // keyboard adjust
        addBtn = findViewById(R.id.addBtn);
        listView = findViewById(R.id.listView);
        itemEt = findViewById(R.id.itemEt);
        arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, items);
        listView.setAdapter(arrayAdapter);

        listLongClick();
        appSwitched = false;
    }

    public void addBtnClick(View view)
    {
        if(itemEt.getText().toString().trim().length() > 0){
            items.add( itemEt.getText().toString() );
            arrayAdapter.notifyDataSetChanged();
            itemEt.setText("");
        }
    }

    public void clearBtnClick(View view)
    {
        items.clear();
        arrayAdapter.notifyDataSetChanged();
    }

    public void listLongClick()
    {
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final int listItem = position;
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(ShoppingList.this);
                alertDialog.setIcon(android.R.drawable.ic_delete);
                alertDialog.setTitle(getString(R.string.shopalert));
                alertDialog.setMessage(getString(R.string.shopalert2));
                alertDialog.setNegativeButton(getString(R.string.no), null);
                alertDialog.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        items.remove(listItem);
                        arrayAdapter.notifyDataSetChanged();
                    }
                });
                alertDialog.show();
                return false;
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        SharedPreferences sp = getSharedPreferences("start", MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("array_size", items.size());

        for(int i=0;i<items.size();i++)
        {
            editor.putString("item_key" + "_" + i, items.get(i));
        }
        editor.commit();
        Log.d("change", "onStop");
    }

   @Override
    protected void onPostResume() {
        super.onPostResume();
        if (appSwitched != true)
        {
            SharedPreferences sp = getSharedPreferences("start", MODE_PRIVATE);
            int arraySize = sp.getInt("array_size", 0);

            for(int i=0;i<arraySize;i++)
            {
                String temp = sp.getString("item_key" + "_" + i, "");
                items.add(temp);
                arrayAdapter.notifyDataSetChanged();
            }
        }
        Log.d("change", "onPostResume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        appSwitched = true;
        Log.d("change", "onRestart");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("change", "onStart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("change", "onDestroy");
    }

    @Override
    protected void onPause() {
        super.onPause();

        SharedPreferences sp = getSharedPreferences("start", MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        Log.d("change", "onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("change", "onResume");
    }
} /*ShoppingList*/