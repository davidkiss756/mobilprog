package com.example.multiapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Locale;


public class Timer extends AppCompatActivity {

    private TextView countdowntxt,input;
    private Button startbtn,resetbtn;

    private CountDownTimer countDownTimer;
    private long timeinmilliseconds;
    private boolean running = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);

        countdowntxt = findViewById(R.id.time);
        input = findViewById(R.id.input);
        startbtn = findViewById(R.id.startbtn);
        resetbtn = findViewById(R.id.resetbtn);

        startbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(running){
                    pauseTimer();
                }else{
                    startTimer();
                }
            }
        });

        resetbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countdowntxt.setText("");
                timeinmilliseconds = 0;
                running = false;
                countDownTimer.cancel();
                startbtn.setText("Start");
            }
        });
    }

    private void startTimer(){
        timeinmilliseconds = Long.parseLong(input.getText().toString());
        countDownTimer = new CountDownTimer(timeinmilliseconds, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timeinmilliseconds = millisUntilFinished;
                updateText();
            }

            @Override
            public void onFinish() {
                running = false;
                startbtn.setText("Start");
                MediaPlayer mp = MediaPlayer.create(Timer.this, R.raw.alarm);
                mp.start();
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(Timer.this);
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setTitle(getString(R.string.timealert));
                alertDialog.setMessage(getString(R.string.timealert2));
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mp.stop();
                    }
                });
                alertDialog.show();


            }
        }.start();

        running = true;
        startbtn.setText("Pause");
    }

    private void pauseTimer(){
        countDownTimer.cancel();
        running = false;
        startbtn.setText("Start");
    }

    private void updateText(){
        int minutes = (int) (timeinmilliseconds / 1000) / 60;
        int seconds = (int) (timeinmilliseconds / 1000) % 60;

        String timeFormatted = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);

        countdowntxt.setText(timeFormatted);
    }
}