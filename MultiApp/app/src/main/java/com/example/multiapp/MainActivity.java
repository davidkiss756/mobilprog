package com.example.multiapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void MainActivityClick(View v)
    {
        Intent intent = new Intent(this, Calculator.class);
        startActivity(intent);
    }

    public void flashlightBtnClick(View v)
    {
        Intent intent = new Intent(this, Flashlight.class);
        startActivity(intent);

    }

    public void airplaneModeBtnClick(View v)
    {
        Intent intent = new Intent(this, WirelessConnectivity.class);
        startActivity(intent);

    }

    public void shoppingListBtnClick(View view)
    {
        Intent intent = new Intent(this, ShoppingList.class);
        startActivity(intent);
    }

    public void compassBtnClick(View view)
    {
        Intent intent = new Intent(this,Compass.class);
        startActivity(intent);
    }

    public void calendarBtnClick(View view)
    {
        Intent i = new Intent(Intent.ACTION_MAIN);
        i.addCategory(Intent.CATEGORY_APP_CALENDAR);
        startActivity(i);
    }

    public void unitConverterBtnClick(View view)
    {
        Intent intent = new Intent(this,UnitConverter.class);
        startActivity(intent);
    }

    public void timerBtnClick(View view)
    {
        Intent intent = new Intent(this,Timer.class);
        startActivity(intent);
    }

} /*MainActivity*/