package com.example.multiapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

public class Flashlight extends AppCompatActivity {

    boolean isFlashlightOn;
    Switch flashSwitch;
    ImageView torchImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flashlight);

        flashSwitch = findViewById(R.id.flashSwitch);
        torchImage = findViewById(R.id.torchImage);

        flashOff();
        flashSwitch.setChecked(false);
        isFlashlightOn = false;

        torchImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFlashlightOn == false)
                {
                    flashOn();
                    torchImage.setImageDrawable(getDrawable(R.drawable.flashon));
                    Snackbar.make(v, getString(R.string.flon), Snackbar.LENGTH_SHORT).show();
                    flashSwitch.setChecked(true);
                }
                else
                {
                    flashOff();
                    torchImage.setImageDrawable(getDrawable(R.drawable.flashoff));
                    Snackbar.make(v, getString(R.string.floff), Snackbar.LENGTH_SHORT).show();
                    flashSwitch.setChecked(false);
                }
            }
        });
    }

    public void flashSwitchBtnClick(View view)
    {
        if (flashSwitch.isChecked())
        {
            flashOn();
            torchImage.setImageDrawable(getDrawable(R.drawable.flashon));
            Snackbar.make(view, getString(R.string.flon), Snackbar.LENGTH_SHORT).show();
        }
        else
        {
            flashOff();
            torchImage.setImageDrawable(getDrawable(R.drawable.flashoff));
            Snackbar.make(view, getString(R.string.floff), Snackbar.LENGTH_SHORT).show();
        }
    }

    public void flashOn()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            CameraManager cameraManager = (CameraManager) getSystemService(this.CAMERA_SERVICE);
            String cameraId;
            try {
                cameraId = cameraManager.getCameraIdList()[0];
                cameraManager.setTorchMode(cameraId, true);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                Toast.makeText(this, getString(R.string.flerror), Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        isFlashlightOn = true;
    }

    public void flashOff()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            CameraManager cameraManager = (CameraManager) getSystemService(this.CAMERA_SERVICE);
            String cameraId;
            try {
                cameraId = cameraManager.getCameraIdList()[0];
                cameraManager.setTorchMode(cameraId, false);
            } catch(IllegalArgumentException e) {
                Toast.makeText(this, getString(R.string.flerror), Toast.LENGTH_LONG).show();
            } catch (CameraAccessException e) {
                e.printStackTrace();
            } catch(RuntimeException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        isFlashlightOn = false;
    }
}