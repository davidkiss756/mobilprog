package com.example.multiapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.Image;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class Compass extends AppCompatActivity {

    ImageView compass;
    TextView latitudeTV, longitudeTV, degreeTV, orientationTV;
    SensorManager sensorManager;
    Sensor sensorAccelerometer;
    Sensor sensorMagneticField;
    float[] floatGravity = new float[3];
    float[] floatGeoMagnetic = new float[3];

    float[] floatOrientation = new float[3];
    float[] floatRotationMatrix = new float[9];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compass);

        compass = findViewById(R.id.compass);
        degreeTV = findViewById(R.id.degree);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        sensorAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorMagneticField = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
    }

    SensorEventListener sensorEventListenerAccelerometer = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {
            floatGravity = sensorEvent.values;

            SensorManager.getRotationMatrix(floatRotationMatrix, null, floatGravity, floatGeoMagnetic);
            SensorManager.getOrientation(floatRotationMatrix, floatOrientation);

            compass.setRotation((float) (-floatOrientation[0] * 180 / 3.14159));
            float currentRotation = compass.getRotation();
            if (currentRotation<(float) 0)
            {
                degreeTV.setText(String.format("%.2f", -currentRotation) + "°");
            }
            else if(currentRotation == (float) -0)
            {
                degreeTV.setText("0°");
            }
            else
            {
                degreeTV.setText(String.format("%.2f", 360 - currentRotation) + "°");
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {

        }
    };

    SensorEventListener sensorEventListenerMagneticField = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {
            floatGeoMagnetic = sensorEvent.values;

            SensorManager.getRotationMatrix(floatRotationMatrix, null, floatGravity, floatGeoMagnetic);
            SensorManager.getOrientation(floatRotationMatrix, floatOrientation);

            compass.setRotation((float) (-floatOrientation[0] * 180 / 3.14159));
            float currentRotation = compass.getRotation();
            if (currentRotation<(float) 0)
            {
                degreeTV.setText(String.format("%.2f", -currentRotation) + "°");
            }
            else if(currentRotation == (float) -0)
            {
                degreeTV.setText("0°");
            }
            else
            {
                degreeTV.setText(String.format("%.2f", 360 - currentRotation) + "°");
            }


        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {
        }
    };



    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(sensorEventListenerAccelerometer, sensorAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(sensorEventListenerMagneticField, sensorMagneticField, SensorManager.SENSOR_DELAY_NORMAL);
    }


    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(sensorEventListenerAccelerometer);
        sensorManager.unregisterListener(sensorEventListenerMagneticField);
    }
}