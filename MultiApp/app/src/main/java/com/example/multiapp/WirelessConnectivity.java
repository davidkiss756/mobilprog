package com.example.multiapp;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class WirelessConnectivity extends AppCompatActivity {

    Button bluetoothOnBtn, bluetoothOffBtn;
    BluetoothAdapter bluetoothAdapter;

    Button wlanOnBtn, wlanOffBtn;
    WifiManager wifiManager;

    Button checkConnectionBtn;
    boolean isConnected;

    ConnectionCheckThread thread = new ConnectionCheckThread();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wireless_connectivity);

        bluetoothOnBtn = findViewById(R.id.bluetoothOnBtn);
        bluetoothOffBtn = findViewById(R.id.bluetoothOffBtn);
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();


        wlanOnBtn = findViewById(R.id.wlanOnBtn);
        wlanOffBtn = findViewById(R.id.wlanOffBtn);

        wifiManager = (WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        checkConnectionBtn = findViewById(R.id.checkConnectionBtn);
        isConnected = false;

        thread.start();
    }

    public void bluetoothOnBtnClick(View view)
    {
        try {
            if (bluetoothAdapter == null) {throw new NullPointerException();}
            if (bluetoothAdapter.isEnabled() == false) //E/BluetoothAdapter: Bluetooth binder is null
                bluetoothAdapter.enable();


            Toast.makeText(this, "Bluetooth ON", Toast.LENGTH_SHORT).show();
        } catch(NullPointerException e)
        {
            e.printStackTrace();
            Toast.makeText(this, "NO Bluetooth device detected", Toast.LENGTH_SHORT).show();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void bluetoothOffBtnClick(View view)
    {
        try {
            if (bluetoothAdapter == null) {throw new NullPointerException();}
            if (bluetoothAdapter.isEnabled() == true)
                bluetoothAdapter.disable();
            Toast.makeText(this, "Bluetooth OFF", Toast.LENGTH_SHORT).show();
        } catch (NullPointerException e) {
            e.printStackTrace();
            Toast.makeText(this, "NO Bluetooth device detected", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void wlanOnBtnCLick(View view)
    {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q )
        {
            wifiManager.setWifiEnabled(true);
            Toast.makeText(this, "WLAN ON", Toast.LENGTH_SHORT).show();
        }
        else
        {
            Toast.makeText(this, "This feature is not available in android 10 or higher", Toast.LENGTH_SHORT).show();
        }
    }

    public void wlanOffBtnClick(View view)
    {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q )
        {
            wifiManager.setWifiEnabled(false);
            Toast.makeText(this, "WLAN OFF", Toast.LENGTH_SHORT).show();
        }
        else
        {
            Toast.makeText(this, "This feature is not available in android 10 or higher", Toast.LENGTH_SHORT).show();
        }
    }

    public void checkConnectionBtnClick(View view)
    {
        checkConnection();
        if (isConnected)
        {
            Toast.makeText(this, getString(R.string.wirok), Toast.LENGTH_SHORT).show();
            checkConnectionBtn.setBackgroundColor(Color.rgb(15,157,88));
        }
        else
        {
            Toast.makeText(this, getString(R.string.wirnotok), Toast.LENGTH_SHORT).show();
            checkConnectionBtn.setBackgroundColor(Color.rgb(219,68,55));
        }
    }

    public boolean checkConnection()
    {
        ConnectivityManager connectivityManager =
                (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo= connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected())
        {
            isConnected = true;
        }
        else
        {
            isConnected = false;
        }
        return isConnected;
    }

    class ConnectionCheckThread extends Thread{

        public void run()
        {
            try {
                if (Thread.interrupted()) { throw new InterruptedException();} // catch alt
                while (Thread.currentThread().isInterrupted() == false)
                {
                    checkConnection();

                    if (isConnected)
                    {
                        checkConnectionBtn.setBackgroundColor(Color.rgb(15,157,88));
                    }
                    else
                    {
                        checkConnectionBtn.setBackgroundColor(Color.rgb(219,68,55));
                    }
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            if (isAlive()) return;
        }
    };
    //Interrupt thread
    @Override
    protected void onPause() {
        super.onPause();
        thread.interrupt();
        Log.d("change", "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        thread.interrupt();
        Log.d("change", "onPostResume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        try
        {
            thread.start();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        Log.d("change", "onRestart");
    }

    @Override
    protected void onStart() {
        super.onStart();
        try
        {
            thread.start();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        Log.d("change", "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("change", "onResume");
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Log.d("change", "onPostResume");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("change", "onDestroy");
    }

    //TODO replace def exception
} /*AirplaneMode*/
