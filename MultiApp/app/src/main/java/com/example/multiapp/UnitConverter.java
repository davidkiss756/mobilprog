package com.example.multiapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;

public class UnitConverter extends AppCompatActivity {

    TextView inputg,inputdkg,inputkg,
             inputcm,inputdm,inputm,
             inputcm2,inputdm2,inputm2,
             inputcm3,inputdm3,inputm3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unit_converter);

        inputg=findViewById(R.id.inputg);
        inputdkg=findViewById(R.id.inputdkg);
        inputkg=findViewById(R.id.inputkg);

        inputcm=findViewById(R.id.inputcm);
        inputdm=findViewById(R.id.inputdm);
        inputm=findViewById(R.id.inputm);

        inputcm2=findViewById(R.id.inputcm2);
        inputdm2=findViewById(R.id.inputdm2);
        inputm2=findViewById(R.id.inputm2);

        inputcm3=findViewById(R.id.inputcm3);
        inputdm3=findViewById(R.id.inputdm3);
        inputm3=findViewById(R.id.inputm3);

        inputkg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().length() > 0){
                    double dkg = Double.parseDouble(s.toString())*100;
                    inputdkg.setText(String.valueOf(dkg));
                    double g = Double.parseDouble(s.toString())*1000;
                    inputg.setText(String.valueOf(g));
                }else{
                    inputdkg.setText("");
                    inputg.setText("");
                }
            }
        });

        inputm.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().length() > 0){
                    double dm = Double.parseDouble(s.toString())*10;
                    inputdm.setText(String.valueOf(dm));
                    double cm = Double.parseDouble(s.toString())*100;
                    inputcm.setText(String.valueOf(cm));
                }else{
                    inputdm.setText("");
                    inputcm.setText("");
                }
            }
        });

        inputm2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().length() > 0){
                    double dm = Double.parseDouble(s.toString())*100;
                    inputdm2.setText(String.valueOf(dm));
                    double cm = Double.parseDouble(s.toString())*10000;
                    inputcm2.setText(String.valueOf(cm));
                }else{
                    inputdm2.setText("");
                    inputcm2.setText("");
                }
            }
        });

        inputm3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().length() > 0){
                    double dm = Double.parseDouble(s.toString())*1000;
                    inputdm3.setText(String.valueOf(dm));
                    double cm = Double.parseDouble(s.toString())*1000000;
                    inputcm3.setText(String.valueOf(cm));
                }else{
                    inputdm3.setText("");
                    inputcm3.setText("");
                }
            }
        });

    }
}