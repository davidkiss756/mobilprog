package com.example.multiapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class Calculator extends AppCompatActivity {

    TextView result,tempTV;
    String temp = "0";
    String currentOperation = "0";
    Double finalValue = (double) 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);

        result = findViewById(R.id.result);
        tempTV = findViewById(R.id.temp);
    }

    private void addNumber(String buttonValue)
    {

        if(result.getText().toString().equals("0") || Double.parseDouble(result.getText().toString().replace(',','.')) == finalValue) {
            result.setText(buttonValue);
        }
        else
            result.append(buttonValue);
    }

    //0, 1, ..., 9 gombok
    public void numberClick(View v)
    {
        Button b = (Button) v;
        String buttonValue = b.getText().toString();
        if (result.getText().toString().length() <= 12){
            addNumber(buttonValue);
        }
        if (result.getText().toString().length() == 9)
        {
            result.setTextSize(50);
        }

    }

    public void clear(View v)
    {
        result.setText("0");
        result.setTextSize(65);
        temp = "0";
        tempTV.setText("");
    }


    public void comma(View v)
    {
        if (!result.getText().toString().contains(",") && result.getText().toString().length() <= 12){
             result.append(",");
        }
    }

    public void plusMinus(View v)
    {
        if (!result.getText().toString().contains("-")){
            result.setText("-"+result.getText().toString());
        }
        else {
            result.setText(result.getText().toString().substring(1));
        }
    }

    public void add(View v)
    {
        temp = result.getText().toString();
        currentOperation = "add";
        result.setText("0");
        tempTV.setText(temp + " +");
    }

    public void div(View v)
    {
        temp = result.getText().toString();
        currentOperation = "div";
        result.setText("0");
        tempTV.setText(temp + " ÷");
    }

    public void mult(View v)
    {
        temp = result.getText().toString();
        currentOperation = "mult";
        result.setText("0");
        tempTV.setText(temp + " ×");
    }

    public void sub(View v)
    {
        temp = result.getText().toString();
        currentOperation = "sub";
        result.setText("0");
        tempTV.setText(temp + " -");
    }

    public void percent(View v)
    {
        Double tempResult = Double.parseDouble(result.getText().toString()) / 100;
        result.setText(tempResult.toString());
    }

    public void equals(View v)
    {
        Double a,b;
        String tempDot = temp.replace(',','.');
        String resultDot = result.getText().toString().replace(',','.');

        a = Double.parseDouble(tempDot);
        b = Double.parseDouble(resultDot);


        switch (currentOperation){
            case "add": finalValue = a + b;
            break;
            case "div": finalValue = a / b;
            break;
            case "mult": finalValue = a * b;
            break;
            case "sub": finalValue = a - b;
            break;
        }
        DecimalFormatSymbols comma = new DecimalFormatSymbols();
        comma.setDecimalSeparator(',');
        comma.setGroupingSeparator('.');
        DecimalFormat format = new DecimalFormat("0.#######", comma);
        result.setText(format.format(finalValue));
        tempTV.setText("");
    }

} /*Calculator*/